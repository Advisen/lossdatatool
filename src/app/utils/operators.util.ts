export const isBlank = (v: string): boolean => (!v || v.trim() === '');
export const isUnDef = (v) => (!v);
export const paramJoin = (v: string[]): string => ( v ? v.join('/') : '');
export const safeSlice = (v: any[], s: number, e: number): any => ( v ? v.slice(s, e) : []);
export const isArrayUnDef = (v: any[]): boolean => (!v || v.length === 0);
