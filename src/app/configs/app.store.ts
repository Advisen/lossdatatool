import { Injectable, ModuleWithProviders } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { ActionReducerMap, StoreModule } from '@ngrx/store';

import * as effects from '../effects';
import { IAppState } from '../models';
import * as reducer from '../reducers';

const STORE_REDUCERS: ActionReducerMap<IAppState> = {
    cases: reducer.caseReducer,
};

const STORE_EFFECTS: any[] = [
    effects.CaseEffects,
];

export const AppNgRxModules: ModuleWithProviders[] = [
    StoreModule.forRoot(STORE_REDUCERS),
    EffectsModule.forRoot(STORE_EFFECTS),
];
