import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Params, PRIMARY_OUTLET, Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';
import { IBreadcrumbOptions } from '../../models';

@Component({
  selector: 'sd-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadCrumbComponent implements OnInit, OnDestroy {
  public breadcrumbs: IBreadcrumbOptions[] = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  public ngOnInit(): void {
    this.filterRouterEvents();
  }

  public ngOnDestroy(): void {
    this.breadcrumbs = null;
  }

  /**
   * Filters the router events to build the breadcrumb.
   */
  private filterRouterEvents(): void {
    this.router.events.filter((e) => e instanceof NavigationEnd).subscribe((e) => {
      const root: ActivatedRoute = this.activatedRoute.root;
      this.breadcrumbs = this.getBreadcrumbs(root);
    });
  }

  /**
   * Returns array of IBreadcrumbOptions that represent the breadcrumb.
   * @param {ActivatedRoute} route
   * @param {string} url
   * @param {Array<IBreadycrumbOptions>} breadcrumbs
   * @returns {Array<IBreadycrumbOptions>} array of breadcrumbs.
   */
  private getBreadcrumbs(route: ActivatedRoute, url: string = '',
    breadcrumbs: IBreadcrumbOptions[] = []): IBreadcrumbOptions[] {
    const ROUTE_DATA_BREADCRUMB = 'breadcrumb';
    const children: ActivatedRoute[] = route.children;

    if (children.length === 0) {
      return breadcrumbs;
    }

    for (const child of children) {
      if (child.outlet !== PRIMARY_OUTLET) {
        continue;
      }

      if (!child.snapshot.data.hasOwnProperty(ROUTE_DATA_BREADCRUMB)) {
        return this.getBreadcrumbs(child, url, breadcrumbs);
      }

      const routeURL: string = child.snapshot.url.map((segment) => segment.path).join('/');
      url += `/${routeURL}`;
      const breadcrumb: IBreadcrumbOptions = {
        label: child.snapshot.data[ROUTE_DATA_BREADCRUMB],
        params: child.snapshot.params,
        url,
      };
      breadcrumbs.push(breadcrumb);
      return this.getBreadcrumbs(child, url, breadcrumbs);
    }
  }
}
