import { ErrorHandler, Inject, Injectable  } from '@angular/core';
import * as StackTrace from 'stacktrace-js';
import { ILOGGER_OPTIONS, ILoggerOptions } from './logger.model';

@Injectable()
export class LoggerService {
    constructor(@Inject(ILOGGER_OPTIONS) private logger: ILoggerOptions) {}

    /**
     * Logs the message as debug
     * @param msg debug message description
     */
    public async debug(msg: string): Promise<void>  {
        // tslint:disable-next-line:no-console
        console.debug(`DEBUG: ${msg}`);
    }

    /**
     * Logs the message as info
     * @param msg info message description
     */
    public async info(msg: string): Promise<void> {
        // tslint:disable-next-line:no-console
        console.info(`INFO: ${msg}`);
    }

    /**
     * Logs the message as warn
     * @param msg warn message description
     */
    public async warn(msg: string): Promise<void> {
        console.warn(`WARN: ${msg}`);
    }

    /**
     * Logs the error
     * @param error detailed error
     */
    public async error(error: Error): Promise<void> {
        console.error(error);
    }
}
