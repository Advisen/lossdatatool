import { Component } from '@angular/core';
import { environment } from '../environments/environment';

import { Store } from '@ngrx/store';
import { AppService } from './services/app.service';

@Component({
  selector: 'sd-root',
  animations: [],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor() { }
}
