import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { isArrayUnDef } from '../../utils';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[requireMatch]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: RequireMatchDirective,
        multi: true,
    }],
})
export class RequireMatchDirective implements Validator {
    @Input('requireMatch') requireMatch: string[];

    validate(c: AbstractControl): { [key: string]: any; } {
        const value = c.value;
        const index = isArrayUnDef(this.requireMatch) ? -1 :
                     (this.requireMatch.findIndex((f) =>
                     f.toLocaleLowerCase() === value.toLocaleLowerCase()));
        return (index !== -1) ? null : {
            requireMatch: index !== -1,
        };
    }
}
