import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { APP_CONSTANTS } from '../../app.const';
import { IAppState } from '../../models/appstate.model';

@Component({
    selector: 'sd-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss'],
})
export class SideNavComponent implements OnInit, OnDestroy {
    @ViewChild('sideNav') sideNavCtrl: ElementRef;
    constructor(private store: Store<IAppState>,
        private router: Router) { }

    /**
     * Cleanup just before the component destroys.
     */
    ngOnDestroy(): void {
    }

    /**
     * Initialize the component.
     */
    public ngOnInit(): void {
        this.mapStore();
    }

    /**
     * Map the store to properties.
     */
    private mapStore(): void {
    }
}
