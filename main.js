/**
 * This file is for electron.
 * 
 */

const { app, BrowserWindow } = require('electron');
let win;

function createWindow () {  
  win = new BrowserWindow({
    width: 600, 
    height: 600,
    backgroundColor: '#ffffff',
    icon: `file://${__dirname}/dist/assets/logo.png`
  })
  win.loadURL(`file://${__dirname}/dist/index.html`)  
  win.on('closed', function () {
    win = null
  })
}

function windowAllClosed () {
    if (process.platform !== 'darwin') {
        app.quit()
      }
}

function activate () {
    if (win === null) {
        createWindow()
      }
}


app.on('ready', createWindow);
app.on('window-all-closed', windowAllClosed);
app.on('activate', activate);