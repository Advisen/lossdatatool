import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[sd-host]',
})
export class AdDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
