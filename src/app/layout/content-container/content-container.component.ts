import { Component, OnInit} from '@angular/core';
import { LoggerService } from '../../blocks/logger/logger.service';

@Component({
    selector: 'sd-content-container',
    templateUrl: './content-container.component.html',
    styleUrls: ['./content-container.component.scss'],
})
export class ContentContainerComponent implements OnInit {
    constructor() { }

    public ngOnInit(): void {
    }
}
