const replace = require('replace-in-file');
const package = require('../package.json');
const buildNumber = process.argv[2];
const options = {
    files: [
            'src/environments/environment.prod.ts',
            'src/environments/environment.ts'
           ],
    from: /buildVersion: '(.*)'/g,
    to: "buildVersion: '" + buildNumber + "'",
    allowEmptyPaths: false
};

try {
    let changedFiles = replace.sync(options);
    if (changedFiles == 0) {
        throw "Please make sure that file '" + options.files + "' has \"buildVersion: ''\"";
    }    
}
catch (err) {
    console.error('Error occurred:', err);
    throw err
}
