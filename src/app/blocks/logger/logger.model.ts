import { InjectionToken } from '@angular/core';

export interface ILoggerOptions {
    level?: LogLevel[]
}

export enum LogLevel {
    Debug = 1,
    Info = 2,
    Warn = 3,
    Error = 4,
}

export const ILOGGER_OPTIONS = new InjectionToken<ILoggerOptions>('work.stream.logger.config');
