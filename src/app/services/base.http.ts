import { HttpClient, HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { Optional } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { paramJoin } from '../utils';

export abstract class  BaseHttp {
    public baseUrl: string = environment.restServiceBaseUrl;
    constructor(private http: HttpClient) {}

    /**
     * Retrieves the data from rest service.
     * @param url
     * @param input
     */
    get<T>(url: string, params?: string[]): Observable<T> {
        return this.http.get<T>(`${this.baseUrl}/${url}/${paramJoin(params)}`, {
                observe: 'response',
                responseType: 'json',
            })
            .map(this.handleResponse)
            .pipe(
                catchError(this.handleError),
            );
    }

    post<T>(url: string, data: any): Observable<T> {
        return this.http.post<T>(`${this.baseUrl}/${url}`, data, {
                observe: 'response',
                responseType: 'json',
            })
            .map(this.handleResponse)
            .pipe(
                catchError(this.handleError),
            );
    }

    put<T>(url: string, params: string[], data: any): Observable<T> {
        return this.http.put<T>(`${this.baseUrl}/${url}/${paramJoin(params)}`, data, {
            observe: 'response',
            responseType: 'json',
        })
        .map(this.handleResponse)
        .pipe(
            catchError(this.handleError),
        );
    }

    delete() {}

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            return new ErrorObservable(`An erorr occurred: ${error.error.message}`);
        } else {
          return new ErrorObservable(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
    }

    private handleResponse<T>(response: HttpResponse<T>) {
        return response.body as T;
    }
}
