import { Injectable } from '@angular/core';
import { AdDirective } from './ad-host.directive';
import { RequireMatchDirective } from './require-match.directive';

export const DIRECTIVES: Injectable[] = [
    AdDirective,
    RequireMatchDirective,
];
