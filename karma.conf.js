// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-phantomjs-launcher'),
      require('karma-coverage-istanbul-reporter'),    
      require('karma-htmlfile-reporter'),
      require('@angular/cli/plugins/karma'),
      require('core-js')
    ],
    client:{
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },    
    angularCli: {
      environment: 'dev'
    },
    reporters: ['progress', 'coverage-istanbul', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome', 'PhantomJS'],
    singleRun: false,
    htmlReporter: {
      outputFile: 'build/reports/unit-tests.html',       
      pageTitle: 'Study Setup Unit Test',
      subPageTitle: 'Unit Test Results',
      groupSuites: true,
      useCompactStyle: true,
      useLegacyStyle: true
    },
    coverageIstanbulReporter: {
      reports: [ 'text-summary', 'lcovonly'],
      fixWebpackSourcePaths: true,
      dir: 'build/reports/code-coverage'
    },
  });
};