import { Params } from '@angular/router';

export interface IBreadcrumbOptions {
    label: string;
    params?: Params;
    url: string;
}
