import { Action } from '@ngrx/store';

class LoadCasesAction implements Action {
    type: string;
    constructor() { }
}

class LoadCasesSuccessfulAction implements Action {
    type: string;
    constructor(public payload: any[]) { }
}

class LoadCasesFailureAction implements Action {
    type: string;
    constructor() { }
}

export enum ActionType {
    LOAD_CASES = 'LOAD_CASES',
    LOAD_CASES_SUCCESSFUL = 'LOAD_CASES_SUCCESSFUL',
    LOAD_CASES_FAILURE = 'LOAD_CASES_FAILURE',
}

export type Action
    = LoadCasesAction
    | LoadCasesSuccessfulAction
    | LoadCasesSuccessfulAction;
