
export const APP_CONSTANTS = {
    SIDE_NAV: {
        FULL_WIDTH: '225px',
        SHORT_WIDTH: '0px',
    },
    AUTO_COMPLETE_SIZE: 500,
};
