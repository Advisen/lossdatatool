import { Injectable } from '@angular/core';
import { NoDataComponent } from './components/no-data/no-data.component';
import { DIRECTIVES } from './directives';

export const SHARED_MODULES: Injectable[] = [
    NoDataComponent,
    DIRECTIVES,
];
