import { ModuleWithProviders } from '@angular/core';

/** Logger Configuration Start */
import { ILoggerOptions, LogLevel } from '../blocks/logger/logger.model';
import { LoggerModule } from '../blocks/logger/logger.module';
const APP_LOGGER_OPT: ILoggerOptions = {
    level: [LogLevel.Debug],
};
export const AppLoggerModule: ModuleWithProviders = LoggerModule.forRoot(APP_LOGGER_OPT);
/** Logger Configuration End */
