import * as casesActions from '../actions';
import { IAppState } from '../models/index';

export function caseReducer(state: any[], action: casesActions.Action): any[] {
    switch (action.type) {
        case casesActions.ActionType.LOAD_CASES: {
            return state;
        }

        case casesActions.ActionType.LOAD_CASES_FAILURE: {
            return state;
        }

        case casesActions.ActionType.LOAD_CASES_SUCCESSFUL: {
            return state;
        }

        default: return state;
    }
}
