import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { IAppState } from '../../models/appstate.model';

@Component({
    selector: 'sd-progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.scss'],
})
export class ProgressbarComponent implements OnInit {
    constructor(public store: Store<IAppState>) { }

    ngOnInit() {
    }
}
