import { Injectable } from '@angular/core';

import { AppService } from './app.service';

export const SERVICES: Injectable[] = [
    AppService,
];
