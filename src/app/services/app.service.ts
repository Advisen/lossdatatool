import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { IAppState } from '../models/appstate.model';

@Injectable()
export class AppService {
    constructor(private store: Store<IAppState>) {
    }
}
