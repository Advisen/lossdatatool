import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LoggerService } from './logger.service';

@Injectable()
export class ErrorHandleService implements ErrorHandler {

    constructor(private injector: Injector) {

    }

    /**
     * Captures all global error occured in the application.
     * @param error details of exception.
     */
    handleError(error: any): void {
        const loggerService = this.injector.get(LoggerService);
        loggerService.error(error);
    }
}
