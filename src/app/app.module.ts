import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { APP_CONFIGURATION } from './configs';
import { HTTP_INTERCEPTOR_PROVIDERS } from './http-interceptors';
import { LAYOUT_COMPONENTS } from './layout';
import { SERVICES } from './services';
import { SHARED_MODULES } from './shared';
import { DIRECTIVES } from './shared/directives';

@NgModule({
  declarations: [
    AppComponent,
    LAYOUT_COMPONENTS,
    SHARED_MODULES,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    APP_CONFIGURATION,
    StoreDevtoolsModule.instrument(),
    BrowserAnimationsModule,
    NoopAnimationsModule,
  ],
  providers: [
    HTTP_INTERCEPTOR_PROVIDERS,
    SERVICES,
    DIRECTIVES,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
