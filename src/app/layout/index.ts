import { ActionsComponent } from './action/action.component';
import { BreadCrumbComponent } from './breadcrumb/breadcrumb.component';
import { ContentContainerComponent } from './content-container/content-container.component';
import { ProgressbarComponent } from './progress-bar/progress-bar.component';
import { SideNavComponent } from './sidenav/sidenav.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

export const LAYOUT_COMPONENTS: any = [
    ToolbarComponent,
    ActionsComponent,
    BreadCrumbComponent,
    ContentContainerComponent,
    ProgressbarComponent,
    SideNavComponent,
];
