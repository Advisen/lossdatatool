import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const APP_ROUTES: Routes = [

];

export const AppRouteModule: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
