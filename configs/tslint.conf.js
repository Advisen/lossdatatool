const tslintHtmlReprot = require('tslint-html-report');
tslintHtmlReprot({
    tslint: 'node_modules/tslint-html-report/tslint.json', // path to tslint.json
    srcFiles: 'src/**/*.ts', // files to lint
    outDir: 'build/reports', // output folder to write the report to.
    html: 'tslint-report.html', // name of the html report generated
    breakOnError: false, // Should it throw an error in tslint errors are found.
    typeCheck: true, // enable type checking. requires tsconfig.json
    tsconfig: 'tsconfig.json'
});