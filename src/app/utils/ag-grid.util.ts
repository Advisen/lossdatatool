/**
 * Build the ag-grid column.
 */
export const buildGridColumn = (field: string, headerName: string,
                                filter: ColFilter = ColFilter.Text,
                                cellClass: string = ''): any => {
        return {
            headerName,
            field,
            cellClass,
            filter,
        } as any;
};

export enum ColFilter {
    Text = 'agTextColumnFilter',
    Number = 'agNumberColumnFilter',
    Date = 'agDateColumnFilter',
}
