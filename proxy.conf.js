'use strict';

const proxy = require('https-proxy-agent');

const proxyConfig = [{
    context: '/api',
    target: '', //'https://pmops-dev.ent.covance.com',
    changeOrigin: true,
    logLevel: 'debug',
}];

module.exports = proxyConfig;