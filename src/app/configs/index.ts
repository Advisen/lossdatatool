import { ModuleWithProviders } from '@angular/core';
import { AppLoggerModule } from './app.config';
import { MATERIAL_MODULE } from './app.material';
import { AppRouteModule } from './app.routes';
import { AppNgRxModules } from './app.store';

export const APP_CONFIGURATION: any[] = [
    AppNgRxModules,
    AppRouteModule,
    MATERIAL_MODULE,
    AppLoggerModule,
];
