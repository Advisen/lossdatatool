import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject } from 'rxjs/';
import { Observable } from 'rxjs/Observable';
import { finalize } from 'rxjs/operators/finalize';
import { tap } from 'rxjs/operators/tap';
import { IAppState } from '../models/appstate.model';

/**
 * Progress Interceptor should spy on the progress of http calls and report back to application layer.
 */
@Injectable()
export class ProgressInterceptor implements HttpInterceptor {

    constructor(private store: Store<IAppState>) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req)
            .pipe(
            tap(
                (resp) => {
                    //
                },
                (err) => {

                },
                () => {

                },
            ),
            finalize(() => {

            }),
        );
    }
}
