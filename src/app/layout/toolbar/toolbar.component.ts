import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { IAppState } from '../../models/appstate.model';

@Component({
    selector: 'sd-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
    constructor(private store: Store<IAppState>,
        private router: Router) { }

    ngOnInit() { }

    /**
     * Toggles the side navigation.
     */
    public toggleSidNav(): void {

    }

    /**
     * LogOut the user from the system.
     */
    public logOut(): void {
    }
}
