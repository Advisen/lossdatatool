import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { APP_CONSTANTS } from '../../app.const';
import { LoggerService } from '../../blocks/logger/logger.service';
import { IAppState } from '../../models/appstate.model';

@Component({
    selector: 'sd-action',
    templateUrl: './action.component.html',
    styleUrls: ['./action.component.scss'],
})
export class ActionsComponent implements OnInit, OnDestroy {
    constructor() { }

    public ngOnInit(): void {
    }

    public ngOnDestroy(): void {
    }
}
