import { CommonModule } from '@angular/common';
import { ErrorHandler, ModuleWithProviders, NgModule } from '@angular/core';

import { ErrorHandleService } from './error-handle.service';
import { ILOGGER_OPTIONS, ILoggerOptions } from './logger.model';
import { LoggerService } from './logger.service';

@NgModule({
    imports: [ CommonModule ],
})
export class LoggerModule {
    static forRoot(config: ILoggerOptions): ModuleWithProviders {
        return {
            ngModule: LoggerModule,
            providers: [
                LoggerService,
                { provide: ILOGGER_OPTIONS, useValue: config },
                { provide: ErrorHandler, useClass: ErrorHandleService },
            ],
        };
    }
}
